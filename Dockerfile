FROM ruby:2.7.1-alpine

RUN apk add --update --no-cache \
  bash \
  binutils-gold \
  build-base \
  curl \
  file \
  g++ \
  gcc \
  git \
  less \
  libstdc++ \
  libffi-dev \
  libc-dev \
  linux-headers \
  libxml2-dev \
  libxslt-dev \
  libgcrypt-dev \
  make \
  netcat-openbsd \
  openssl \
  pkgconfig \
  postgresql-dev \
  python \
  tzdata

COPY Gemfile* /usr/src/app/
WORKDIR /usr/src/app

ENV BUNDLE_PATH /gems

RUN gem update bundler

RUN bundle install

COPY . /usr/src/app/

COPY wait-for.sh /wait-for

RUN chmod +x /wait-for

RUN chmod +x /usr/src/app/docker-entrypoint.sh

ENTRYPOINT ["./docker-entrypoint.sh"]

CMD ["/bin/bash", "-c", "bin/rails server -b 0.0.0.0 -p $PORT"]