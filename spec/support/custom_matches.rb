RSpec::Matchers.define :have_error_message do |message|
  match do |body|
    expect(JSON.parse(body))
         .to include({
           'status' => 'error',
           'message' => message
         })
  end
end