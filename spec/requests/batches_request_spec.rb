require 'rails_helper'

RSpec.describe "Batches", type: :request do

  let(:valid_orders){
    4.times.map do |i|
      create(:order, purchase_channel: 'SBR', delivery_service: '', reference: i.to_s)
     end
  }

  let(:batch_factory){
    ->(status) {
      batch = create(:batch, orders: valid_orders, purchase_channel: 'SBR')
      batch.orders.update(status: status)
      batch
    }
  }

  describe "GET /show" do
    it "returns the batch" do
      get show_batch_url(batch_factory.(:production))
      expect(response).to be_successful
    end
  end

  describe "POST /create" do

    shared_examples 'a batch validation error' do |message|
      it "should fail, showing a validation error" do

        expect {

          post create_batch_url, params: req_params

        }.to change(Batch, :count).by 0

        expect(response).to have_http_status(:bad_request)

        expect(response.body).to have_error_message(message)
      end
    end

    context "when orders passed pass all validations" do

      let!(:orders){ valid_orders }
      let(:params) {
        { orders: [ orders.pluck(:reference) ], purchase_channel: 'SBR' }
      }

      it "creates a new Batch and passes it's orders to 'production' " do

        expect {

          post create_batch_url, params: params

        }.to change(Batch, :count).by 1

        batch = Batch.last

        orders.each &:reload

        expect(batch.orders.count).to eq orders.count

        orders.each { |order| expect(order).to be_production }

        expect(response).to have_http_status(:created)
      end

    end

    context "when some orders are not ready" do
      let!(:orders) {
        batch = Batch.new purchase_channel: 'SBR'
        batch.orders = valid_orders
        batch.save
        batch.orders << create(:order, purchase_channel: 'SBR', delivery_service: 'SEDEX', reference: '5', status: 'production', batch: batch)

        batch.orders
      }

      let(:req_params) {
        { orders: [ orders.pluck(:reference) ], purchase_channel: 'SBR' }
      }

     it_behaves_like 'a batch validation error', 'Some orders are already on the pipeline'

    end

    context "when some orders are not found" do
      let!(:orders) { valid_orders }
      let(:req_params) {
        {
          orders: orders.pluck(:reference).push('5'),
          purchase_channel: 'SBR'
        }
      }

      it_behaves_like 'a batch validation error', "Couldn't find some orders"
    end

    context "when some orders don't belong to the same purchase_channel" do
      let!(:orders) {
        valid_orders.push create(:order, purchase_channel: 'Site 2 BR', delivery_service: 'SEDEX', reference: '5')
      }

      let(:req_params) {
        { orders: [ orders.pluck(:reference) ], purchase_channel: 'SBR' }
      }

      it_behaves_like 'a batch validation error', 'Some orders don\'t belong to the channel SBR'

    end
    context "when no orders are passed" do
      let(:req_params) {
        { orders: [], purchase_channel: 'SBR'}
      }

      it_behaves_like 'a batch validation error', {"orders"=>["can't be empty"]}

    end
  end

  shared_examples 'batch not_found' do
    it "should show a not_found response" do
      post produce_batch_url('1'), params: { }
      expect(response).to have_http_status(:not_found)
    end

  end

  describe "post /produce" do

    context "when the batch does not exist" do
      it_behaves_like 'batch not_found'
    end

    context "when the batch exists" do

      let!(:batch) { batch_factory.(:production) }

      context "when the orders are not production" do
        it "will show a request error" do

          batch.orders.update(status: :closing)

          post produce_batch_url(batch.reference)

          expect(response).to have_http_status(:bad_request)
          expect(response.body).to have_error_message('invalid pipeline transition: orders are closing')
        end
      end

      it "marks the orders on the batch as produced (closing) " do
        post produce_batch_url(batch.reference)

        expect(response).to have_http_status(:created)
        expect(response.body).to be_present

        batch_orders = batch.orders.reload
        batch_orders.each { |order| expect(order).to be_closing }
      end
    end
  end

  describe "post /send" do

    context "when the batch does not exist" do
      it_behaves_like 'batch not_found'
    end

    let!(:batch) { batch_factory.(:closing) }

    context "when orders on batch have the status closing and delivery service is provided" do
      it "marks the orders on the batch as sent" do
        post send_batch_url(batch.reference), params: { delivery_service: 'Sedex' }

        expect(response).to have_http_status(:created)

        batch_orders = batch.orders.reload

        batch_orders.each { |order| expect(order).to be_sent }
        batch_orders.each { |order| expect(order.delivery_service).to eq 'SEDEX'}
      end

    end

    context "when delivery service is not provided" do
      it "will show a bad request" do
        post send_batch_url(batch.reference)

        expect(response).to have_http_status(:bad_request)
      end
    end

    context "when orders are not closing" do

      it "will show a request error" do

        batch.orders.update_all(status: :sent)

        post send_batch_url(batch.reference), params: { delivery_service: 'Sedex' }

        expect(response).to have_http_status(:bad_request)
        expect(response.body).to have_error_message('invalid pipeline transition: orders are already sent')
      end
    end
  end
end
