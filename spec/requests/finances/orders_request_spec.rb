require 'rails_helper'

RSpec.describe "Finances::Orders", type: :request do
  describe "/GET index" do
    it "shows a simple financial report" do
      get finances_orders_path

      expect(response).to have_http_status :ok
    end
  end
end
