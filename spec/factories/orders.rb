FactoryBot.define do
  factory :order do

    reference {'BR33'}
    purchase_channel { 'SITE BR'}
    client_name { 'AUTO_FORCE' }
    client_address { 'Av. Amintas Barros Nº 3700 - Torre Business, Sala 702 - Lagoa Nova CEP: 59075-250'}
    total_value { 300.0 }
    line_items { [] }

  end
end
