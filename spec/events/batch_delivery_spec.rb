require 'rails_helper'

describe BatchDelivery do

  let!(:batch){
    order = create(:order)
    batch = BatchCreation.new([BatchNilPolicy]).fire([order.reference], order.purchase_channel)
    BatchProduction.new.fire(batch)
    batch.reload
  }
  it "fails if no Delivery Service is provided" do
    batch.orders.update(status: :closing)

    expect { subject.fire(batch)}.to broadcast(:batch_delivery_error, 'Delivery Service cannot be blank')
  end
end