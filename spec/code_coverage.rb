unless ENV['NO_COVERAGE']
  require 'simplecov'

  ignored_coverage_files = [
    'app/channels/application_cable/connection.rb',
    'app/channels/application_cable/channel.rb',
    'app/jobs/application_job.rb',
    'app/mailers/application_mailer.rb'
  ]

  SimpleCov.start 'rails' do
    ignored_coverage_files.each { |file| add_filter file }
  end
end
