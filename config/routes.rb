Rails.application.routes.draw do

  resources :orders, param: :reference

  get 'batches/:reference', to: 'batches#show', as: :show_batch
  post 'batches/create', to: 'batches#create', as: :create_batch
  post 'batches/:reference/produce', to: 'batches#produce', as: :produce_batch
  post 'batches/:reference/send', to: 'batches#send_batch', as: :send_batch
  # violating conventions because of ruby's send method

  namespace :finances do
    resources :orders, only: [:index]
  end
end
