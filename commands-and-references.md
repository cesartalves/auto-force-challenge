# Commands and References

I find it useful to keep a list of used commands and references for my tests!

## Commands

- rails new . --api --database=postgresql

- rails g rspec:install
- rails g scaffold Order reference:string:uniq purchase_channel:string:index client_name:string client_address:text delivery_service:string:index total_value:decimal line_items:text, status:integer

- rails g model Batch reference:string:uniq purchase_channel:string
- rails g migration AddBatchToOrders batch:references
- rails g controller Batches

- rails g factory_bot:model Batch

- rails g controller SimpleFinancialReport

- heroku addons:add heroku-postgresql

-  Batch.last.orders += Order.where(purchase_channel: "SITE BR")

## References

- https://stackoverflow.com/questions/8514167/float-vs-decimal-in-activerecord
- https://api.rubyonrails.org/v5.2.3/classes/ActiveRecord/Enum.html
- https://nandovieira.com.br/usando-postgresql-e-jsonb-no-rails
- https://edgeguides.rubyonrails.org/active_record_postgresql.html
- https://guides.rubyonrails.org/action_controller_overview.html#rescue-from
- https://gist.github.com/be9/6446051
- https://blog.bigbinary.com/2016/02/15/rails-5-makes-belong-to-association-required-by-default.html
- https://edgeguides.rubyonrails.org/active_record_validations.html#performing-custom-validations
- https://relishapp.com/rspec/rspec-expectations/v/2-4/docs/custom-matchers/define-matcher
- https://medium.com/@roanmonteiro/criando-uma-api-do-design-at%C3%A9-a-seguran%C3%A7a-8129933c4e08
- https://support.atlassian.com/bitbucket-cloud/docs/configure-bitbucket-pipelinesyml/
