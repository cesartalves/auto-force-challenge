class OrderDestroyal
  include Wisper::Publisher

  def fire(order)

    order.destroy
    broadcast :order_destroyed, order
  end
end