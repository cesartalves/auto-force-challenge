class BatchCreation
  include Wisper::Publisher

  def initialize(order_validation_policies)
    @order_validation_policies = order_validation_policies
  end

  def fire(order_references, purchase_channel)

    orders = Order.lock.where(reference: order_references)
    batch = Batch.new purchase_channel: purchase_channel

    begin
      @order_validation_policies.each do |policy|
        policy.validate order_references, orders, purchase_channel
      end
    rescue BatchValidationError => error
      return broadcast(:batch_creation_error, error.message, error.orders)
    end

    batch.transaction do

      batch.orders = orders

      if batch.save

        orders.update(status: :production)

        broadcast(:batch_created, batch.reload)
        batch
      else
        broadcast(:batch_creation_error, batch.errors, [])
      end
    end
  end
end