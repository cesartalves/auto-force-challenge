class BatchProduction
  include Wisper::Publisher

  def fire(batch)
    order = batch.orders.lock.last

    unless order.production?
      return broadcast(:batch_production_error, "invalid pipeline transition: orders are #{order.status}")
    end

    batch.produce

    broadcast :batch_produced, batch: batch.reload
  end

end