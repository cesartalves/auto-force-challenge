class OrderCreation
  include Wisper::Publisher

  def fire(order_params)

    @order = Order.new(order_params)

    if @order.save
      broadcast :order_created, @order
      return @order
    end

    broadcast :order_creation_error, @order.errors
  end
end