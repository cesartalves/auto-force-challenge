class BatchDelivery
  include Wisper::Publisher

  def fire(batch)
    order = batch.orders.lock.last

    unless order.closing?
      return broadcast(:batch_delivery_error, 'invalid pipeline transition: orders are already sent')
    end

    if batch.delivery_service.blank?
      return broadcast(:batch_delivery_error, 'Delivery Service cannot be blank')
    end

    batch.send_batch

    broadcast :batch_delivered, batch: batch.reload

  end
end