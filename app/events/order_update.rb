class OrderUpdate
  include Wisper::Publisher

  def fire(order, order_params)

    if order.update(order_params)
      broadcast :order_updated, order
      return order
    end

    broadcast :order_update_error, order.errors
  end
end