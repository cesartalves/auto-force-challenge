class BatchOrderPurchaseChannelPolicy

  def validate(order_references, orders, batch_purchase_channel)

    orders_without_valid_purchase_channel = orders.where.not(purchase_channel: batch_purchase_channel)

    if orders_without_valid_purchase_channel.present?
      invalid_orders = orders_without_valid_purchase_channel.pluck(:reference)

      raise BatchValidationError.new(message % batch_purchase_channel, invalid_orders)
    end
  end

  private

  def message
    "Some orders don't belong to the channel %s"
  end
end