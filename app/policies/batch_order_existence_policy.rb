class BatchOrderExistencePolicy

  def validate(order_references, orders, batch_purchase_channel)
    @inexistent_orders = order_references - orders.pluck(:reference)

    # this is the case where an empty array of orders is passed. Prefered to validate this one using ActiveRecord's validation
    @inexistent_orders.reject! &:blank?

    raise BatchValidationError.new(message, @inexistent_orders) if @inexistent_orders.present?
  end

  private

  def message
    'Couldn\'t find some orders'
  end
end