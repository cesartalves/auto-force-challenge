class BatchOrderReadinessPolicy

  def validate(order_references, orders, batch_purchase_channel)

    orders_which_arent_ready = orders.where.not(status: :ready)
                                     .or(orders.where.not(batch_id: nil))

    if orders_which_arent_ready.present?
      raise BatchValidationError.new(message, orders_which_arent_ready.pluck(:reference))
    end
  end

  private

  def message
    'Some orders are already on the pipeline'
  end

end