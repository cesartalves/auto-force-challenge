class BatchSerializer < ActiveModel::Serializer
  attributes :id, :reference, :purchase_channel, :links, :status

  include Rails.application.routes.url_helpers

  def status; object.status; end

  def links
    return [ show_batch_link, produce_batch_link ] if object.production?
    return [ show_batch_link, send_batch_link ] if object.closing?

    [ show_batch_link ]

  end

  has_many :orders

  private

  def show_batch_link
    {
      href: show_batch_path(object),
      rel: 'show batch',
      type: 'GET'
    }
  end

  def produce_batch_link
    {
      href: produce_batch_path(object),
      rel: 'produce batch',
      type: 'POST'
    }
  end

  def send_batch_link
    {
      href: send_batch_path(object),
      rel: 'send batch',
      type: 'POST'
    }
  end
end
