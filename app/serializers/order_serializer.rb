class OrderSerializer < ActiveModel::Serializer
  attributes :reference, :status, :purchase_channel, :client_name, :client_address, :delivery_service, :total_value, :batch_id, :line_items, :links

  include Rails.application.routes.url_helpers

  def links
    [ { href: order_path(object), type: 'GET' } ]
  end

  # has_one :batch
end
