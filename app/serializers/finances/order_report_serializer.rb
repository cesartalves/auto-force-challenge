class Finances::OrderReportSerializer < ActiveModel::Serializer
  attributes :total_value, :purchase_channel, :orders
end