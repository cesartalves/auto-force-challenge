class Finances::OrdersService

  def on_order_created(order)
    params = order.slice :reference, :total_value, :purchase_channel

    Finances::Order.create params
  end

  def on_order_destroyed(order)
    Finances::Order.find(order.reference).destroy
  end

  def on_order_updated(order)
    order = Finances::Order.find(order.reference)
    order.update order.slice :total_value, :purchase_channel
  end
end