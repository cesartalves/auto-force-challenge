class BatchValidationError < Exception

  attr_reader :orders

  def initialize(message, orders)
    super message
    @orders = orders
  end

end