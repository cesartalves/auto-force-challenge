module OrderValidations
  extend ActiveSupport::Concern

  included do
    validates_presence_of :purchase_channel
    validates_presence_of :reference
    validates_presence_of :client_name
    validates_presence_of :client_address
    validates_presence_of :delivery_service, if: -> { sent? }
    validates_presence_of :batch_id, if: -> { production? || closing? || sent? }
    validates_presence_of :total_value
    validates :total_value, numericality: { greater_than_or_equal_to: 0 }
  end
end