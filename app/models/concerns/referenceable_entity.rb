module ReferenceableEntity
  extend ActiveSupport::Concern

  included do
    validates_uniqueness_of :reference

    def to_param; reference; end
  end
end