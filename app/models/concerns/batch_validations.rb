module BatchValidations
  extend ActiveSupport::Concern

  included do
    validates_presence_of :purchase_channel
    validate :orders_cannot_be_empty

    protected

    def orders_cannot_be_empty
      if orders.empty?
        errors.add(:orders, "can't be empty")
      end
    end

  end
end