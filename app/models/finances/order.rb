class Finances::Order < ApplicationRecord
  self.primary_key = 'reference'

  validates_uniqueness_of :reference
  validates_presence_of :purchase_channel
  validates_presence_of :total_value
end
