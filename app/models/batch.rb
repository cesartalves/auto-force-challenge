require 'reference_generator'

class Batch < ApplicationRecord

  include ReferenceGenerator
  include BatchValidations
  include ReferenceableEntity

  has_many :orders

  before_save { self.purchase_channel = purchase_channel.upcase }
  before_validation :set_random_reference

  delegate :status, :ready?, :production?, :closing?, :sent?, to: :last_order

  def last_order; orders.last; end

  def produce
    orders.update(status: :closing)
  end

  def send_batch
    orders.update(status: :sent, delivery_service: @delivery_service)
  end

  def delivery_service
    last_order.delivery_service.presence || @delivery_service
  end

  def delivery_service=(delivery_service)
    @delivery_service = delivery_service
  end

  private

  def set_random_reference
    loop do

      reference = generate_reference

      if Batch.find_by_reference(reference).blank?
        self.reference = reference
        break
      end
    end
  end

end
