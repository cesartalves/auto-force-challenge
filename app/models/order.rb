class Order < ApplicationRecord

  include OrderValidations
  include ReferenceableEntity

  belongs_to :batch, optional: true

  enum status: [
    :ready, # a new order, ready to be produced
    :production, # waiting to be printed
    :closing, # already produced, waiting to be sent
    :sent, # on the way to the client
  ]

  before_save {
    self.purchase_channel = purchase_channel.upcase
    self.delivery_service = delivery_service.upcase if delivery_service.present?
  }

  protected

  # would like to protected the method "update" so that only batch orders can do it. But this would require serious monkeypatching ;("
  # for future reference
  # https://github.com/rails/rails/blob/master/activerecord/lib/active_record/relation.rb

end
