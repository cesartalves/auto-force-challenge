class Finances::OrdersController < ApplicationController

  def index
    permitted_params = params.permit(:purchase_channel)

    @report = Finances::Order.select("SUM (total_value) as total_value", "COUNT(reference) as Orders", :purchase_channel)
                 .where(permitted_params)
                 .group(:purchase_channel)

    render json: @report, each_serializer: Finances::OrderReportSerializer
  end
end
