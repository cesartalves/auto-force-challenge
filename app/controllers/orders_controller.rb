class OrdersController < ApplicationController

  before_action :set_order, only: [:show, :update, :destroy]
  rescue_from ActionController::ParameterMissing, with: :missing_order

  def index
    permitted_filters = params.permit :client_name, :status, :id, :reference, :purchase_channel

    default_orders_per_page = 50

    page = params[:page].presence || 1
    per_page = params[:per_page].presence || default_orders_per_page

    @orders = Order.where(permitted_filters)

    result = paginate @orders, per_page: per_page, page: page

    render json: result
  end

  def show
    render json: @order
  end

  def create
    event = OrderCreation.new

    event.on(:order_created) { |order| render json: order, status: :created, location: order }
    event.on(:order_creation_error) do |errors|
      render json: error(problems: errors), status: :unprocessable_entity
    end

    event.subscribe Finances::OrdersService.new, prefix: :on

    event.fire order_params
  end

  def update

    event = OrderUpdate.new

    event.on(:order_updated) { |order| render json: order }
    event.on(:order_update_error) { |errors| render json: errors, status: :unprocessable_entity }

    event.subscribe Finances::OrdersService.new, prefix: :on

    event.fire @order, order_params
  end

  def destroy

    event = OrderDestroyal.new

    event.subscribe Finances::OrdersService.new, prefix: :on

    event.fire @order
  end

  private
    def set_order
      @order = Order.find_by_reference(params[:reference])
      render json: error(message: 'order not found : (') if @order.blank?
    end

    def order_params
      params.require(:order).permit(:reference, :purchase_channel, :client_name, :client_address, :delivery_service, :total_value, :line_items, :status)
    end

    def missing_order
      render({
        json: error({
          "message": 'Parameter Order required ;(.',
          "like this!": { order: { reference: 'BR33434', etc: ""}}
        }),
        status: :bad_request})
    end
end
