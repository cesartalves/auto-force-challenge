class BatchesController < ApplicationController

  rescue_from ActionController::ParameterMissing, with: :parameter_missing_error
  before_action :set_batch, only: [ :show, :produce, :send_batch ]

  def show
    render json: @batch
  end

  def create

    orders           =  params.require :orders
    purchase_channel =  params.require :purchase_channel

    event            =  BatchCreation.new batch_creation_policies

    event.on(:batch_created) do |batch|
      render status: :created, json: { batch: serialize(batch), total_orders: batch.orders.count }
    end

    event.on(:batch_creation_error) do |error, orders|

      response_body = { message: error }
      response_body.merge!({ orders: orders }) if orders.present?

      render_bad_request response_body
    end

    event.fire orders, purchase_channel
  end

  def produce

    event = BatchProduction.new

    event.on(:batch_production_error) { |message| render_bad_request message: message }
         .on(:batch_produced)         { |batch:| render status: :created, json: batch }

    event.fire @batch
  end

  def send_batch

    @batch.delivery_service = params.require(:delivery_service)

    event = BatchDelivery.new

    event.on(:batch_delivery_error)  { |message| render_bad_request message: message }
         .on(:batch_delivered)       { |batch:| render status: :created, json: batch }

    event.fire @batch
  end

  protected

  def render_bad_request(body)
    render status: :bad_request, json: error(body)
  end

  private

  def set_batch
    reference = params.require :reference
    @batch = Batch.find_by(reference: reference)

    render status: :not_found if @batch.blank?
  end

  def batch_creation_policies(policies = nil)
    @batch_creation_policies = policies || [ BatchOrderExistencePolicy.new, BatchOrderPurchaseChannelPolicy.new, BatchOrderReadinessPolicy.new ]
  end

  def parameter_missing_error(exception)
    render_bad_request message: exception.message
  end

end
