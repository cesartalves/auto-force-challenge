class ApplicationController < ActionController::API

  protected

  def paginate(active_relation, page:1, per_page: 20)
    collection = active_relation.page(page).per(per_page)

    current, total, per_page = collection.current_page, collection.total_pages, collection.limit_value

    {
      pagination: {
        current:  current,
        previous: (current > 1 ? (current - 1) : nil),
        next:     (current == total ? nil : (current + 1)),
        per_page: per_page,
        pages:    total,
        total:    collection.total_count
        },
      result: serialize(collection)
    }
  end

  def serialize(serializable_resource)
    ActiveModelSerializers::SerializableResource.new(serializable_resource).as_json
  end

  def error(json)
    { status: 'error' }.merge json
  end
end
