# Auto Force Challenge

[![time tracker](https://wakatime.com/badge/bitbucket/cesartalves/auto-force-challenge.svg)](https://wakatime.com/badge/bitbucket/cesartalves/auto-force-challenge)
[![Heroku](https://heroku-badge.herokuapp.com/?app=auto-force-challenge&root=orders)](https://auto-force-challenge.herokuapp.com/orders)

## Briefing Summary

[API Documentation](https://documenter.getpostman.com/view/8797843/Szzq3urY?version=latest "API doc")

REST API

- Receive Purchase Orders
- Group them on Batches
- Follow the Orders in the **production pipeline**

## Setting up

- ./setup.sh

## Running

- docker-compose up -d (binds to localhost 3001)

## Running tests

- rspec
- ./run-container-tests.sh (runs tests on docker container)

## How would I implement?

### A security layer || Permission layer

Check this Pull Request

https://bitbucket.org/cesartalves/auto-force-challenge/pull-requests/1/security-layer-permissions-layer/diff

Since the challenge is a Contract First API, I have kept this Contract Last implementation rather simple :)

### Modify orders

Can already be done! Check API

### A web UI

Probably just generate the views with a simple bootstrap template. There's a gem called rails admin that can do that https://github.com/sferik/rails_admin. But doing this from scratch doesn't seem to be too complicated

### Financial reports

In this PR, I separate the financial report into it's own bounded context

https://bitbucket.org/cesartalves/auto-force-challenge/pull-requests/2/separate-financial-report-into-its-own

## ~~TODO~~

### Entities

#### ~~Order~~

#### ~~Batch~~

### ~~Critical business rules~~

- Batches must be grouped by Purchase Channels. Don't mix orders from the different Purchase Channels in the same Batch
- Orders with the same Delivery Service are dispatched together

### Actions / Routes

#### Order

#### create

- ~~input data for an Order. Return whether Order's valid and if it was persisted~~

#### get_status:

- ~~find by reference~~
- ~~find by name (of the client )~~
- ~~long term clients may have many orders - pagination might be a must~~

#### orders by purchase channel:
- ~~filter by status~~

#### create a batch:
- ~~input orders. Return reference to the batch and number of orders~~

#### produce a batch

- ~~pass batch from production to `closing`~~

#### close a batch

- ~~pass batch and Delivery Service. Mark batch / orders as `sent`~~

### Financial Report

- ~~Show, for each Purchase Channel, how many Orders and Total Value (Sum) for those Orders.~~

### Extras

#### ~~Authentication layer~~

#### ~~Permissions layer~~

### Known issues

- Pagination returns next / previous even if there're no records 😅



