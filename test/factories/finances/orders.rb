FactoryBot.define do
  factory :finances_order, class: 'Finances::Order' do
    reference { "MyString" }
    value { "9.99" }
    purchase_channel { "MyString" }
  end
end
