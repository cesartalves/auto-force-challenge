class AddBatchToOrders < ActiveRecord::Migration[6.0]
  def change
    add_reference :orders, :batch, foreign_key: true
  end
end
