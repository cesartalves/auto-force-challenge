class CreateFinancesOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :finances_orders, id: false, primary_key: :reference do |t|
      t.string :reference
      t.decimal :total_value
      t.string :purchase_channel

      t.timestamps
    end
    add_index :finances_orders, :reference, unique: true
    add_index :finances_orders, :purchase_channel
  end
end
