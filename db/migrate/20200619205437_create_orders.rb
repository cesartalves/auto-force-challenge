class CreateOrders < ActiveRecord::Migration[6.0]
  def change
    create_table :orders do |t|
      t.string :reference
      t.string :purchase_channel
      t.string :client_name
      t.text :client_address
      t.string :delivery_service
      t.decimal :total_value
      t.jsonb :line_items, null: false, default: []
      t.integer :status, null: false, default: 0

      t.timestamps
    end
    add_index :orders, :reference, unique: true
    add_index :orders, :purchase_channel
    add_index :orders, :delivery_service
  end
end
