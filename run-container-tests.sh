#!/bin/bash

docker-compose run --rm -v $PWD:/usr/src/app -e RAILS_ENV=test web \
  bundle exec rspec