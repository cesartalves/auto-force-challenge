module ReferenceGenerator
  def generate_reference(random_segment_size=6)
    random_part = SecureRandom.alphanumeric random_segment_size

    "BR-#{random_part}"
  end
end