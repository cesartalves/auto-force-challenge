#!/bin/bash

touch .env
docker-compose run --rm migrations bash -c \
  'bundle install &&
   bin/rails db:create &&
   bin/rails db:migrate'
